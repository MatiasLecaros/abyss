﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public GameObject hazard;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;
	public GUIText vidasText;

	public int score;
	public static int vidas=3;
	private bool gameOver;
	private bool restart;


	void Start()
	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		updateScore ();
		StartCoroutine	(spawnWaves ());
		if(vidas==0) vidas=3;

	}

	void Update()
	{
		//Reiniciar el juego al apretar la letra R
		if (restart) {
				if (Input.GetKeyDown (KeyCode.R)) {
						Application.LoadLevel (Application.loadedLevel);
				}
		}
		vidasText.text = ": " + vidas;
		//Pasar al siguiente nivel si el jugador llega a cierta posicion x en el mapa
		if (GameObject.FindGameObjectWithTag ("Player")) {
						if (score >= 1 && GameObject.FindGameObjectWithTag ("Player").transform.position.x < -4)
								Application.LoadLevel ("Nivel2");
				}
	}

	//codigo para llamar olas de meteoritos en el tutorial de unity (no se aplica en abyss)
	IEnumerator spawnWaves()
	{
		yield return new WaitForSeconds (startWait);
		while(true){
			for(int i=0;i<hazardCount;i++){
				Vector3 spawnPosition = new Vector3 (Random.Range(-spawnValues.x,spawnValues.x),spawnValues.y,spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity ;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds(waveWait);
			if (gameOver){
				restartText.text = "Press 'R' for restart";
				restart = true;
				break;
			}
		}
	}

	//añadir y actualizar puntaje
	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		updateScore ();
	}

	//actualizar GUI de score 
	void updateScore()
	{
		scoreText.text = ": " + score;
	}


	public void GameOver()
	{	
		vidas -= 1;
		vidasText.text = ": " + vidas;  //actualizar GUI vidas

		Application.LoadLevel (Application.loadedLevel);
		if (vidas == 0) {
						gameOverText.text = "GAME OVER!";
						gameOver = true;
						Application.LoadLevel ("GameOver");
				}
	}

}
