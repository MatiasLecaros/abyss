﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

	public float speed;
	public float tilt;
	public Boundary boundary;
	//public GameObject shot;
	public GameObject baliza;
	public Transform balizaSpawn;
	public float balizaRate;
	public float bateria;
	public float duracionBateria;
	private float durancion_bat_respaldo;
	public float cantidadBalizas;
	private float nextBaliza;
	public float accelerationForce = 3.5f;
	public float rotationForce = 0.02f;
	public float maxRotationVelocity = 5f;
	public GUIText bateriaText;
	private bool aux_bateria = false; 
	private float aux_bate_intensidad;

	//disparar al presionar un boton
	void Update()
	{
		//Dejar baliza en el escenario esperando cierto tiempo entre ellas
		if (Input.GetButton("Fire1") && Time.time > nextBaliza && cantidadBalizas>0 ) {
			nextBaliza = Time.time + balizaRate;
			//Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			Instantiate(baliza, balizaSpawn.position, balizaSpawn.rotation);
			cantidadBalizas--;
			GetComponent<AudioSource>().Play();
		}
		//Fijar rotacion maxima.
		float rotation = Input.GetAxis("Horizontal");
		if (rotation * rotationForce < maxRotationVelocity || rotation * rotationForce > -maxRotationVelocity) {
				GetComponent<Rigidbody>().AddTorque (0, rotation * rotationForce, 0);
		} 
		else {
				GetComponent<Rigidbody>().angularVelocity = GetComponent<Rigidbody>().angularVelocity;			
		}
		//si se apreta boton arriba se acelera
		if (Input.GetKey (KeyCode.UpArrow)) {
						float acceleration = Input.GetAxis ("Vertical");
						GetComponent<Rigidbody>().AddForce (transform.forward * acceleration * accelerationForce); //aceleracion original
				}
		// si se apreta boton abajo se frena la nave
		if (Input.GetKey (KeyCode.DownArrow)) {
			if(GetComponent<Rigidbody>().velocity.magnitude < 0){
				GetComponent<Rigidbody>().velocity = Vector3.zero;
			}
			else{
				GetComponent<Rigidbody>().velocity -= GetComponent<Rigidbody>().velocity*Time.deltaTime*2;
				GetComponent<Rigidbody>().angularVelocity -= GetComponent<Rigidbody>().angularVelocity*Time.deltaTime*2;
			}
		}
	
		//intensidad de la luz disminuye a medida que se agota la bateria.
		bateria = GameObject.FindGameObjectWithTag ("Bateria").GetComponent ("Light").GetComponent<Light>().intensity;
		bateria -= duracionBateria;
		bateriaText.text = "Bateria: "+ Mathf.Ceil(bateria*100.0f/8.0f) +" %";
		GameObject.FindGameObjectWithTag ("Bateria").GetComponent ("Light").GetComponent<Light>().intensity=bateria;
	}

	//la intensidad de la bateria se reestablece al maximo.
	public void AddBateria ()
	{
		GameObject.FindGameObjectWithTag ("Bateria").GetComponent ("Light").GetComponent<Light>().intensity = 8;
		bateria = 8;
	}

	// Update is called once per frame
	void FixedUpdate () {
/*		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
	
		//movimiento de la nave en x y z
		//Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rigidbody.AddRelativeForce (Vector3.forward * moveVertical * 5);
		if(Input.GetKey(KeyCode.D)) {
			gameObject.rigidbody.AddTorque(0,rotationSpeed*Time.deltaTime,0);
		}
	//	rigidbody.velocity = movement * speed;

*/		
		// prevenir que la nave se salga de la pantalla 
		GetComponent<Rigidbody>().position = new Vector3 
		(
			Mathf.Clamp(GetComponent<Rigidbody>().position.x,boundary.xMin,boundary.xMax),
			0.0f, 
			Mathf.Clamp(GetComponent<Rigidbody>().position.z,boundary.zMin,boundary.zMax)
		);

		//Rotacion de la nave al girar a la izquierda o a la derecha
	//	rigidbody.rotation = Quaternion.Euler (0.0f, 0.0f, rigidbody.velocity.x * -tilt);



	}
}
