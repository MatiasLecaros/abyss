﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

//	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private GameController gameController;
	private bool flag;

	void Start()
	{

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null) {
				Debug.Log ("Cannot find 'Gamecontroller' script");
		}
	}
	
	IEnumerator esperar()
	{
		float timeToWait = 4f;
		yield return new WaitForSeconds(timeToWait);
		gameController.GameOver ();


	}
	void OnTriggerEnter(Collider other) {
	//void OnCollisionEnter(Collider other) {
		if (other.tag == "Boundary") {
			return;
		}
//		Instantiate (explosion, transform.position, transform.rotation);
		if (other.tag == "Player") {
			Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
			StartCoroutine(esperar() );
		}
		gameController.AddScore (scoreValue);
		Destroy(other.gameObject);
//		Destroy (gameObject);
	}
}
