﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public GameObject Jugador;
	private Vector3 offset;
	// Use this for initialization
	void Start () {
		if (Jugador != null) {
				offset = transform.position;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Jugador != null) {
						transform.position = Jugador.transform.position + offset;
		}
	}
}
