﻿using UnityEngine;
using System.Collections;

public class SurvivorContact : MonoBehaviour {
		
	public GameObject explosion;
	public int scoreValue;
	public int iluminacion;
	private GameController gameController;
	private PlayerController playerController;
	
	void Start()
	{
		GetComponent<AudioSource>().Play();
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		if (gameController == null) {
			Debug.Log ("Cannot find 'Gamecontroller' script");
		}

		GameObject playerControllerObject = GameObject.FindWithTag ("Player");
		if (playerControllerObject != null) {
			playerController = playerControllerObject.GetComponent<PlayerController>();
		}
		if (playerController == null) {
			Debug.Log ("Cannot find 'Playercontroller' script");
		}
	}
	
	void OnTriggerEnter(Collider other) {

		if (other.tag == "Player") {
			Instantiate (explosion, transform.position, transform.rotation);
			gameController.AddScore (scoreValue);
			playerController.AddBateria();

			Destroy (gameObject);
		}
	}
}
