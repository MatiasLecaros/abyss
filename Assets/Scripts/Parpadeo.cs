﻿using UnityEngine;
using System.Collections;

public class Parpadeo : MonoBehaviour {

	public float waitingTime=0.05f;
	IEnumerator Start ()
	{
		while (true)
		{
			GetComponent<Light>().enabled = !(GetComponent<Light>().enabled); //toggle on/off the enabled property
			yield return new WaitForSeconds(waitingTime);
		}
	}
}
